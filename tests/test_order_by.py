from sqlalchemy_orm.order_by import OrderBy, OrderByDirection

from sqlalchemy_orm.base.base import Model
from sqlalchemy_orm.database import Database


class TestFileController:
    def test_order_by(self):
        Base = Model()

        class Animal(Base):
            name: str
            age: int
            size: int

        db = Database("sqlite:///:memory:")

        db.create(Animal)

        session = db.session()

        animal_1 = Animal(name="bea", age="6", size=10)
        animal_2 = Animal(name="chloe", age="13", size=7)
        animal_3 = Animal(name="poppy", age="7", size=5)
        animal_4 = Animal(name="mikey", age="2", size=1)
        animal_5 = Animal(name="thomas", age="2", size=3)
        animal_6 = Animal(name="sasha", age="9", size=6)
        animal_7 = Animal(name="timmy", age="2", size=5)
        animal_8 = Animal(name="amy", age="2", size=3)

        session.create(animal_1)
        session.create(animal_2)
        session.create(animal_3)
        session.create(animal_4)
        session.create(animal_5)
        session.create(animal_6)
        session.create(animal_7)
        session.create(animal_8)

        animals = session.query(Animal).order_by(OrderBy(key="name")).all()
        animal_names = [animal.name for animal in animals]

        assert animal_names == [
            "amy",
            "bea",
            "chloe",
            "mikey",
            "poppy",
            "sasha",
            "thomas",
            "timmy",
        ]

        animals = (
            session.query(Animal)
            .order_by(
                OrderBy(key="age", direction=OrderByDirection.desc),
                OrderBy(key="size", direction=OrderByDirection.desc),
            )
            .all()
        )

        animal_names = [animal.name for animal in animals]

        assert animal_names == [
            "chloe",
            "sasha",
            "poppy",
            "bea",
            "timmy",
            "thomas",
            "amy",
            "mikey",
        ]
