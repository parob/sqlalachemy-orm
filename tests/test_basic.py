from sqlalchemy import Column, Integer

from sqlalchemy_orm.base.base import Model, ModelBase
from sqlalchemy_orm.database import Database


class TestModel:
    def test_equality(self):
        class Animal(ModelBase):
            name: str
            age: int

            hello = Column(Integer)

        dog = Animal(name="molly", age="10")
        dog2 = Animal(name="bea", age="10")
        dog3 = Animal(name="bea", age="3")
        dog4 = Animal()
        dog5 = Animal(hello=1)

        assert dog != dog2
        assert dog3 == dog2
        assert dog3 != dog4
        assert dog4 == dog5

    def test_session_create_model(self):
        Base = Model()

        class Animal(Base):
            name: str
            age: int

        database = Database("sqlite:///:memory:")
        database.create(Animal)

        session = database.session()

        bea = Animal(name="bea", age=5)
        tom = Animal(name="Tom", age=5)

        assert bea not in session and tom not in session

        session.create(bea)
        session.create(tom)

        assert bea in session
        assert bea == session.query(Animal).filter(Animal.name == "bea").one()

        tom: Animal = session.query(Animal).filter(Animal.name == "Tom").one()
        assert tom.name == "Tom"

        session.rollback()

    def test_db_create_relations(self):
        Base = Model()

        class Animal(Base):
            name: str
            age: int

        class Person(Base):
            name: str
            age: int
            pet: Animal

        db = Database("sqlite:///:memory:")

        assert Animal not in db
        assert Person not in db

        db.create(Person)

        assert Animal in db
        assert Person in db

    def test_dataclass(self):
        Base = Model()

        class AnimalModel(Base):
            name: str
            age: int
            size: int

        class CatModel(AnimalModel):
            color: str

        class DogModel(CatModel):
            age: str
            size: str = 10

        dog = DogModel(name="baxter", age="10", color="yellow", size=11)

        assert dog
