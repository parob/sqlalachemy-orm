from sqlalchemy_orm import Database
from sqlalchemy_orm.base.base import Model


class TestEralchemy:
    def test_eralchemy(self):
        Base = Model()

        class AnimalModel(Base):
            name: str
            age: int
            size: int

        class CatModel(AnimalModel):
            color: str

        class DogModel(CatModel):
            age: str
            size: str = 10

        db = Database("sqlite:///:memory:")

        db.create(DogModel)

        diagram = db.entity_relationship_diagram()

        assert "animalmodel" in diagram
        assert "color" in diagram
        assert "dogmodel" in diagram
